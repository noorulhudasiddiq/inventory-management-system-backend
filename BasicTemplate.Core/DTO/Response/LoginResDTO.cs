﻿using InventoryManagement.Core.DTO.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Core.DTO.Response
{
    public class LoginResDTO
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public bool EmailVerified { get; set; }
        public string ImageUrl { get; set; }
        public bool Status { get; set; }
        public TokenResDTO Token { get; set; }
    }
}
