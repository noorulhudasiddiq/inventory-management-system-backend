﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Core.DTO.Response
{
    public class ProductResDTO
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public float SellingPrice { get; set; }
        public float PurchasePrice { get; set; }
        public int Quantity { get; set; }
        public string Image { get; set; }
    }
}
