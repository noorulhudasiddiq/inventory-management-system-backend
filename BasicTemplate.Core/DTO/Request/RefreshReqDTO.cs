﻿using System;
namespace InventoryManagement.Core.DTO.Request
{
    public class RefreshReqDTO
    {

        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }


    }
}
