﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Core.DTO.Request
{
    public class AddProductReqDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public float SellingPrice { get; set; }
        public float PurchasePrice { get; set; }
        public int Quantity { get; set; }
        public string Image { get; set; }
    }
}
