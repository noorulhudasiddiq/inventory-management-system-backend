﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Core.DTO.Request
{
    public class UpdateUserReqDTO
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}
