﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Core.DTO.Request
{
    public class AddStaffReqDTO
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Phone2 { get; set; }
    }
}
