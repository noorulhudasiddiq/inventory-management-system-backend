﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Core.DTO.Request
{
    public class VerifyForgetPasswordReqDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
