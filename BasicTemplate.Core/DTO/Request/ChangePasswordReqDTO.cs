﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Core.DTO.Request
{
    public class ChangePasswordReqDTO
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
