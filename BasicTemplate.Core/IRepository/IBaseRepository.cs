﻿using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Core.IRepository
{
    public interface IBaseRepository<TEntity, TKey> : IRepository<TEntity, TKey>
    {
    }
}
