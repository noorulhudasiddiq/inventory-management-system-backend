﻿using InventoryManagement.Core.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Core.IRepository
{
    public interface ICustomerRepository : IBaseRepository<Customer, int>
    {
    }
}
