﻿using System;
using InventoryManagement.Core.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace InventoryManagement.Core.Models
{
    public partial class InventoryManagementContext : DbContext
    {
        const string connectionString = "Data Source=DESKTOP-A0OLBID\\NOORDB;Initial Catalog=InvetoryManagementSystem;Integrated Security=True";
        public InventoryManagementContext()
        {
           
        }

        public InventoryManagementContext(DbContextOptions<InventoryManagementContext> options)
            : base(options)
        {
        }

        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<RefreshToken> RefreshToken { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
        }
    }

}