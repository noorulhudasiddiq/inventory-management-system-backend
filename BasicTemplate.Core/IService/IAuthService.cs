﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Interface;

namespace InventoryManagement.Core.IService
{
    public interface IAuthService : IService<IUserRepository, User, int>
    {
        Task<DataTransferObject<bool>> Register( RegisterReqDTO dtoObj);
        Task<DataTransferObject<LoginResDTO>> Login( LoginReqDTO dtoObj);
        Task<DataTransferObject<LoginResDTO>> VerifyUser(VerifyUserReqDTO dtoObj);
        Task<DataTransferObject<bool>> ForgetPassword(string email);
        Task<DataTransferObject<bool>> VerifyForgetPassword(VerifyForgetPasswordReqDTO dtoObj);
        Task<DataTransferObject<bool>> ResendVerificationEmail(string email);
        Task<DataTransferObject<TokenResDTO>> Refresh(RefreshReqDTO tokenObj);
    }
}
