﻿using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Core.IService
{
    public interface IUserService : IService<IUserRepository, User, int>
    {
        Task<DataTransferObject<bool>> ChangePassword(int userID, ChangePasswordReqDTO dtoObj);
        Task<DataTransferObject<LoginResDTO>> UpdateUser(int userID, UpdateUserReqDTO dtoObj);
        Task<DataTransferObject<LoginResDTO>> GetUser(int userID);
        Task<DataTransferObject<bool>> UpdatePushToken(int userID, string pushToken);
        Task<User> FindUserByEmail(string email);


    }
}
