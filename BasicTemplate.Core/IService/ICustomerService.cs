﻿using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Core.IService
{
    public interface ICustomerService : IService<ICustomerRepository, Customer, int>
    {
        Task<DataTransferObject<CustomerResDTO>> AddCustomer(AddCustomerReqDTO dtoObj);
        Task<DataTransferObject<CustomerResDTO>> GetCustomer(int customerID);
        Task<DataTransferObject<List<CustomerResDTO>>> GetAllCustomer();
        Task<DataTransferObject<CustomerResDTO>> UpdateCustomer(int ID, UpdateCustomerReqDTO dtoObj);

        Task<DataTransferObject<CustomerResDTO>> DeleteCustomer(int customerID);
    }
}
