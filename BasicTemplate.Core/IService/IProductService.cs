﻿using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Core.IService
{
    public interface IProductService : IService<IProductRepository, Product, int>
    {
        Task<DataTransferObject<ProductResDTO>> AddProduct(AddProductReqDTO dtoObj);
        Task<DataTransferObject<ProductResDTO>> GetProduct(int productID);
        Task<DataTransferObject<List<ProductResDTO>>> GetAllProduct();
        //Task<DataTransferObject<ProductResDTO>> UpdateProduct(int ID, UpdateStaffReqDTO dtoObj);

        Task<DataTransferObject<ProductResDTO>> DeleteProduct(int productID);
    }
}
