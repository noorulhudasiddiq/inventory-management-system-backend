﻿using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Core.IService
{
    public interface IStaffService : IService<IStaffRepository, Staff, int>
    {
        Task<DataTransferObject<StaffResDTO>> AddStaff(AddStaffReqDTO dtoObj);
        Task<DataTransferObject<StaffResDTO>> GetStaff(int staffID);
        Task<DataTransferObject<List<StaffResDTO>>> GetAllStaff();
        Task<DataTransferObject<StaffResDTO>> UpdateStaff(int ID, UpdateStaffReqDTO dtoObj);

        Task<DataTransferObject<StaffResDTO>> DeleteStaff(int staffID);
    }
}
