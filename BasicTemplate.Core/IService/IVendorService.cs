﻿using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Core.IService
{
    public interface IVendorService : IService<IVendorRepository, Vendor, int>
    {
        Task<DataTransferObject<VendorResDTO>> AddVendor(AddVendorReqDTO dtoObj);
        Task<DataTransferObject<VendorResDTO>> GetVendor(int vendorID);
        Task<DataTransferObject<List<VendorResDTO>>> GetAllVendor();
        Task<DataTransferObject<VendorResDTO>> UpdateVendor(int ID, UpdateVendorReqDTO dtoObj);

        Task<DataTransferObject<VendorResDTO>> DeleteVendor(int vendorID);
    }
}
