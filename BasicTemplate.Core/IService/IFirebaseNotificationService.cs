﻿using InventoryManagement.Common.DTO;
using InventoryManagement.NetCore.Base.Abstract;
using FirebaseAdmin.Messaging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Core.IService
{
    public interface IFirebaseNotificationService
    {
        Task<TopicManagementResponse> SubsribeToTopic(FirebaseTopicSubUnsubDTO dtoObj);
        Task<TopicManagementResponse> UnSubsribeToTopic(FirebaseTopicSubUnsubDTO dtoObj);
        Task<string> SendNotificationToSpecificDevice(FirebaseNotificationToSpecificDeviceDTO dtoObj);
        Task<BatchResponse> SendNotificationToDevices(FirebaseNotificationToDevicesDTO dtoObj);
        Task<string> SendNotificationToTopic(FirebaseNotificationToTopicDTO dtoObj);
        Task<BatchResponse> SendBatchOfNotificationsToTopic(List<FirebaseNotificationToTopicDTO> dtoObj);
    }
}
