﻿using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace InventoryManagement.Core.Entity
{
    public class RefreshToken : IBase<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required]
        public string Token { get; set; }
        [Required]
        public DateTime Expiry { get; set; }

       
        public int UserID { get; set; }
        public User User { get; set; }
    }
}
