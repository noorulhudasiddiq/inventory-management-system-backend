﻿using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace InventoryManagement.Core.Entity
{
    public class Product : IBase<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required]
        [MaxLength(20)]
        public string  Code { get; set; }
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }
        [Required]
        public float SellingPrice { get; set; }
        [Required]
        public float PurchasePrice { get; set; }
        [Required]
        public int Quantity { get; set; }
        public string Image { get; set; }
        [Required]
        public bool isActive { get; set; }

    }
}
