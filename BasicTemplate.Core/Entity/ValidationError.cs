﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryManagement.Core.Models
{
    public class ValidationError
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime Timestamp { get; }

        public string Message { get; }

        public ValidationError(DateTime timestamp, string message)
        {
            this.Timestamp = timestamp;
            this.Message = message;
        }
    }
}
