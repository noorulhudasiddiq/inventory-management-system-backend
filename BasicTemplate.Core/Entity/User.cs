﻿using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace InventoryManagement.Core.Entity
{
    public class User  : IBase<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public bool EmailVerified { get; set; }
        public string TokenActivate { get; set; }
        public string TokenForget { get; set; }
        public string ImageUrl { get; set; }
        public bool Status { get; set; }
        public DateTime DateCreated { get; set; }
        public string PushToken { get; set; }

        public ICollection<RefreshToken> RefreshTokens { get; set; }
    }
}
