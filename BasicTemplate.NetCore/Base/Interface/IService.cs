﻿using InventoryManagement.NetCore.Attributes;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Enum;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.NetCore.Base.Interface
{
    public interface IService
    {
        IUnitOfWork _unitOfWork { get; }
    }

 
    public interface IService<TRepository, TEntity, TKey> : IService
    {
        TRepository Repository { get; }
    }
}
