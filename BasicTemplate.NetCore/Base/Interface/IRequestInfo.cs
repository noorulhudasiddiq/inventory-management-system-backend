﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.NetCore.Base.Interface
{
    public interface IRequestInfo
    {
        DbContext Context { get; }
    }
}
