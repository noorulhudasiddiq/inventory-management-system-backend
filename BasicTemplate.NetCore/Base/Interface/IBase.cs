﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace InventoryManagement.NetCore.Base.Interface
{
    public interface IBase<TKey> : IBase
    {
        [Key]
        TKey ID { get; set; }
    }

    public interface IBase
    {
    }
}

