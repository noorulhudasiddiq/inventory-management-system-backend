﻿using System;
using System.Collections.Generic;
using System.Linq;
using InventoryManagement.Common.Helper;
using InventoryManagement.NetCore.Base.Interface;
using Spark.Common.Helper;

namespace InventoryManagement.NetCore.Base.Abstract
{
    public class DTO<TEntity, TKey> : IBase<TKey>
          where TEntity : IBase<TKey>, new()
    {

        public TKey ID { get; set; }

      
     
        public DTO()
        {

        }

        public DTO(TEntity entity)
        {
            this.ID = entity.ID;
        }

        public TEntity ConvertToEntity()
        {
            TEntity entity = new TEntity();
            return this.ConvertToEntity(entity);
        }

        public virtual TEntity ConvertToEntity(TEntity entity)
        {
            entity.ID = this.ID == null || this.ID.Equals(default(TKey)) ? entity.ID : this.ID;
            return entity;
        }

        public virtual void ConvertFromEntity(TEntity entity)
        {
            this.ID = entity.ID;
        }

        public static List<TDTO> ConvertEntityListToDTOList<TDTO>(IEnumerable<TEntity> entityList) where TDTO : DTO<TEntity, TKey>, new()
        {
            var result = new List<TDTO>();
            if (entityList != null)
                foreach (var entity in entityList)
                {
                    var dto = new TDTO();
                    dto.ConvertFromEntity(entity);
                    result.Add(dto);
                }
            return result;
        }

        public static IList<TEntity> ConvertDTOListToEntity(IEnumerable<DTO<TEntity, TKey>> dtoList, IEnumerable<TEntity> entityList)
        {
            var result = new List<TEntity>();
            if (dtoList != null)
                foreach (var dto in dtoList)
                {
                    var entityFromDb = entityList.SingleOrDefault(x => x.ID.Equals(dto.ID));
                    if (entityFromDb != null)
                    {
                        result.Add(dto.ConvertToEntity(entityFromDb));
                    }
                    else
                    {
                        result.Add(dto.ConvertToEntity());
                    }
                }
            foreach (var entity in entityList.Where(x => !dtoList.Any(y => y.ID.Equals(x.ID))))
            {
                result.Add(entity);
            }
            return result;
        }

        public static IList<TEntity> ConvertDTOListToEntity(IEnumerable<DTO<TEntity, TKey>> dtoList)
        {
            var result = new List<TEntity>();
            if (dtoList != null)
                foreach (var dto in dtoList)
                {
                    result.Add(dto.ConvertToEntity());
                }
            return result;
        }

        public T CopyTo<T>() where T : new()
        {
            return ObjectHelper.CopyObject<T>(this);
        }
    }
}
