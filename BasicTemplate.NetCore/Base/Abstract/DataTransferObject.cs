﻿using System;


namespace InventoryManagement.NetCore.Base.Abstract
{
    public class DataTransferObject<T> : DTOBase
    {
        public DataTransferObject() {

            this.TimeStamp = DateTime.UtcNow;

        }
        public DataTransferObject(T result): this()
        {
            this.Result = result;
        }

        public DataTransferObject(int code, string resultMessage) : this()
        {
            this.code = code;
            this.ResultMessage = resultMessage;
        }

        public DataTransferObject(T result, int code, string resultMessage) : this()
        {
            this.code = code;
            this.Result = result;
            this.ResultMessage = resultMessage;
           
        }

        public DataTransferObject(bool hasError, int code, string errorMessage)
        {
            this.code = code;
            this.HasErrors = hasError;
            this.Error = new Exception(errorMessage);
        }

        public DataTransferObject(bool hasError, string errorMessage)
        {
            this.HasErrors = hasError;
            this.Error = new Exception(errorMessage);
        }

        public string Message { get; set; }
        public DateTime TimeStamp { get; set; }
        public string ResultMessage { get; set; }
        public T Result { get; set; }
        public int code { get; set; }
    }



}
