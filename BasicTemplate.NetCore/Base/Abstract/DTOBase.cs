﻿using System;

namespace InventoryManagement.NetCore.Base.Abstract
{
    public class DTOBase
    {
        public bool HasErrors { get; set; }

        public Exception Error { get; set; }
    }
}
