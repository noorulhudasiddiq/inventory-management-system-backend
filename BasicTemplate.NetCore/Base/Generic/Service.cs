﻿using AutoMapper;
using InventoryManagement.Common.Helper;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.NetCore.Base.Generic
{
    public class Service : IService
    {
        public IUnitOfWork _unitOfWork { get; private set; }

        public Service(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }

    public class Service<TRepository, TEntity, TKey> : Service, IService<TRepository, TEntity, TKey>
     where TEntity : IBase<TKey>, new()
     where TRepository : IRepository<TEntity, TKey>
     where TKey : IEquatable<TKey>
    {
        private readonly TRepository _repository;
        private readonly IMapper _mapper;

        public TRepository Repository => _repository;

        public Service(IUnitOfWork unitOfWork, TRepository repository, IMapper mapper)
            : base(unitOfWork)
        {
            _repository = repository;
            _mapper = mapper;
        }
        protected async Task Delete(TKey id)
        {
            await _repository.DeleteAsync(id);
        }
        public virtual async Task DeleteAsync(TKey id)
        {
            await Delete(id);
            await _unitOfWork.SaveAsync();
        }
        public virtual async Task DeleteAsync(IList<TKey> ids)
        {
            foreach (TKey id in ids)
            {
                await Delete(id);
            }

            _unitOfWork.DBContext.ChangeTracker.AutoDetectChangesEnabled = false;
            await _unitOfWork.SaveAsync();
            _unitOfWork.DBContext.ChangeTracker.AutoDetectChangesEnabled = true;
        }
        public virtual async Task<int> GetCount()
        {
            return await _repository.GetCount();
        }
        public virtual async Task<TEntity> UpdateEntityAsync(TEntity entityObject)
        {
            await _repository.Update(entityObject);
            await _unitOfWork.SaveAsync();
            return entityObject;

        }
        public virtual async Task<IList<TEntity>> UpdateAsync(IList<TEntity> entityObjects)
        {
            List<Task> taskList = new List<Task>();

            foreach (var entityObject in entityObjects)
            {
                taskList.Add(_repository.Update(entityObject));
            }

            await Task.WhenAll(taskList);

            _unitOfWork.DBContext.ChangeTracker.AutoDetectChangesEnabled = false;
            await _unitOfWork.SaveAsync();
            _unitOfWork.DBContext.ChangeTracker.AutoDetectChangesEnabled = true;

            return entityObjects;
        }
        public async Task BulkDelete(IList<TKey> ids)
        {
            foreach (TKey id in ids)
            {
                await Delete(id);
            }

            await _unitOfWork.SaveAsync();
        }


    }
}
