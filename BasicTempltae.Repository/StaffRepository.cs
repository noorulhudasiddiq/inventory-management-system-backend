﻿using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.NetCore.Base.Interface;

using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Repository
{
    public class StaffRepository : BaseRepository<Staff, int>, IStaffRepository
    {
        public StaffRepository(IRequestInfo requestInfo) : base(requestInfo)
        {

        }
    }
}
