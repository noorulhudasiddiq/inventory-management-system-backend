﻿
using InventoryManagement.Core.IRepository;
using InventoryManagement.NetCore.Base.Generic;
using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Repository
{
    public class BaseRepository<TEntity, TKey> : Repository<TEntity, TKey>, IBaseRepository<TEntity, TKey>
         where TEntity : class, IBase<TKey>
            where TKey : IEquatable<TKey>
    {
        public BaseRepository(IRequestInfo requestInfo)
         : base(requestInfo)
        {
        }
    }
}
