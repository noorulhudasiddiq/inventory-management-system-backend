﻿using System;
using System.Net;
using System.Net.Mail;
using TimeZoneConverter;
using static InventoryManagement.Common.Constant;

namespace InventoryManagement.Common.Helper
{
    public class OtherUtilities
    {
        public static bool RemoteFileOk(string Url)
        {
            HttpWebRequest req = WebRequest.Create(Url) as HttpWebRequest;
            req.Method = "HEAD";
            try
            {
                using (HttpWebResponse rsp = req.GetResponse() as HttpWebResponse)
                {
                    return rsp.StatusCode == HttpStatusCode.OK;
                }
            }
            catch (WebException ex)
            {
                return false;
            }
        }
        public static string GetContactType(string contactType, string whatDoYouCallYourClient = "Contact")
        {
            switch (contactType)
            {
                case "P":
                    return ("Prospect");
                case "C":
                    return ("Active " + whatDoYouCallYourClient);
                case "I":
                    return ("Inactive " + whatDoYouCallYourClient);
                case "X":
                    return ("DELETED CONTACT");
                case "F":
                    return ("Former " + whatDoYouCallYourClient);
                case "O":
                    return ("Other Contact");
                case "V":
                    return ("Vendor");

                case "T":
                    return ("On a Trial");

                case "U":
                    return ("Formerly On a Trial");

                case "Q":
                    return ("Former Prospect");

                case "D":
                    return ("After Schooler");

                case "E":
                    return ("Camper");

                case "Z":
                    return ("Fitness");

                case "S":
                    return ("Staff / Employee");

                default:
                    return ("Unknown");

            }
        }
        public static string TimeZoneConvertUTC2Local(DateTime? dateToConvert, string timeZone, bool includeTime, bool returnFormatted = false, string dateFormat = "MDY", bool useShortDate = false)
        {

            string retValue = string.Empty;
            if (dateToConvert == null)
            {
                return string.Empty;
            }
            if (!(dateToConvert is DateTime))
            {
                return string.Empty;
            }
            DateTime theDate = DateTime.Parse(dateToConvert.ToString());
            try
            {
                TimeZoneInfo zone = TZConvert.GetTimeZoneInfo(timeZone);
                string localTime = TimeZoneInfo.ConvertTimeFromUtc(theDate, zone).ToString();
                theDate = DateTime.Parse(localTime);
            }
            catch (Exception)
            {

                return string.Empty;
            }
            if (returnFormatted)
            {
                //   pending;
                retValue = FormatDateTime(theDate, includeTime, dateFormat, useShortDate);
            }
            else
            {
                if (includeTime)
                {
                    retValue = theDate.ToString("MM/dd/yy" + (useShortDate ? "" : "yy") + "hh:mm:ss tt");
                }
                else
                {
                    retValue = theDate.ToString("MM/dd/yy" + (useShortDate ? "" : "yy"));
                }
            }


            return retValue;

        }
        public static string FormatDateTime(object TimeToFormat, bool IncludeTime, string dateFormat, bool useShortDate = false)
        {
            // This is Ron Sell's version of FormatDateTime()

            // Date and Time Strings can be found...
            // http://msdn.microsoft.com/en-us/library/8kb3ddd4.aspx
            // do not do a conversion if were not using a time

            // FIRST MAKE SURE IT'S A LEGIT DATE
            if (string.IsNullOrEmpty(TimeToFormat.ToString()))
            {
                return string.Empty;
            }

            if (TimeToFormat is DateTime)
            {
                if (DateTime.Parse(TimeToFormat.ToString()) < DateTime.Parse("1/2/1900"))
                {
                    // I do not want to display any dates that are 1/1/1900.
                    // 1/1/1900 is our version of "unknown date/null"
                    return string.Empty;
                }
            }







            // NOW LETS FORMAT IT
            string retVal = string.Empty;
            DateTime theDate = DateTime.Parse(TimeToFormat.ToString());
            string strDateFormat = dateFormat;
            if (string.IsNullOrEmpty(strDateFormat))
            {
                strDateFormat = "MDY"; // Default
            }

            switch (strDateFormat)
            {
                case "MDY":
                    {
                        if (IncludeTime)
                        {
                            retVal = theDate.ToString("MM/dd/yy" + (useShortDate ? "" : "yy") + " hh:mm tt");
                        }
                        else
                        {
                            retVal = theDate.ToString("MM/dd/yy" + (useShortDate ? "" : "yy"));
                        }

                        break;
                    }

                case "DMY":
                    {
                        if (IncludeTime)
                        {
                            retVal = theDate.ToString("dd/MM/yy" + (useShortDate ? "" : "yy") + " hh:mm tt");
                        }
                        else
                        {
                            retVal = theDate.ToString("dd/MM/yy" + (useShortDate ? "" : "yy"));
                        }

                        break;
                    }

                case "YMD":
                    {
                        if (IncludeTime)
                        {
                            retVal = theDate.ToString("yy" + (useShortDate ? "" : "yy") + "/MM/dd hh:mm tt");
                        }
                        else
                        {
                            retVal = theDate.ToString("yy" + (useShortDate ? "" : "yy") + "/MM/dd");
                        }

                        break;
                    }

                default:
                    {
                        // Use the USA default
                        if (IncludeTime)
                        {
                            retVal = theDate.ToString("MM/dd/yy" + (useShortDate ? "" : "yy") + " hh:mm tt");
                        }
                        else
                        {
                            retVal = theDate.ToString("MM/dd/yy" + (useShortDate ? "" : "yy"));
                        }

                        break;
                    }
            }

            return retVal;
        }
        public static DateTime GetLastDayInMonth(DateTime date)
        {

            date = DateTime.Parse(date.AddMonths(1).ToString() + "/1/" + date.Year);
            date = date.AddDays(-1);

            return date;
        }
        
        public static bool EmailIsValid(string emailaddress)
        {
            try
            {
                MailAddress mail = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        


    }

}


