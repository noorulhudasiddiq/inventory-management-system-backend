﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Common.DTO
{
    public class FirebaseNotificationToSpecificDeviceDTO
    {
        public string PushToken { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
