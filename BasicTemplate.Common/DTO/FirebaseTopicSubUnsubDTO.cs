﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Common.DTO
{
    public class FirebaseTopicSubUnsubDTO
    {
        public List<string> pushTokens { get; set; }

        public string Topic { get; set; }
    }
}
