﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryManagement.Common.Configuration
{
    public class WebUIConfiguration
    {
        public string WebURL { get; set; }
        public string IoSPasswordURL { get; set; }
    }
}
