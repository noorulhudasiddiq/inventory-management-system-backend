﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using InventoryManagement.Common;

namespace InventoryManagement.Common.Configuration
{
    public class EmailConfiguration
    {
        public static bool SendEmail(string subject, string body, string email)
        {

            try
            {
                var apiKey = Environment.GetEnvironmentVariable("SENDGRID_API_KEY");
                var client = new SendGridClient(apiKey);
                var msg = new SendGridMessage()
                {
                    From = new EmailAddress(Constant.AppConfig.EMAIL, Constant.AppConfig.EMAIL_NAME),
                    Subject = subject,
                    HtmlContent = body
                };
                msg.AddTo(new EmailAddress(email, "Test User"));
                client.SendEmailAsync(msg);
            }
            catch
            {
                return false;
            }

            return true;

        }
    }
}
