﻿using System.Collections.Generic;

namespace InventoryManagement.Common
{
    public static class Constant
    {
        public static class Strings
        {
            public static Dictionary<string, List<string>> FileTypes
            {
                get
                {
                    Dictionary<string, List<string>> retObj = new Dictionary<string, List<string>>();

                    retObj.Add("Document", new List<string>()
                    {
                        "jpg",
                        "jpeg",
                        "png",
                        "gif",
                        "doc",
                        "pdf",
                        "ppt",
                        "xls",
                        "docx",
                        "pptx",
                        "xlsx",
                        "xml",
                        "txt"
                    });

                    retObj.Add("Video", new List<string>()
                    {
                        "wmv",
                        "mp4",
                        "m4a",
                        "m4v"
                    });

                    retObj.Add("Audio", new List<string>()
                    {
                        "mp3"
                    });

                    return retObj;
                }
            }

           
        }

        public static class ResponseMessage
        {
            //User
            public const string USER_EXIST = "User already Exist.";
            public const string USER_REGISTERED = "User registered, check your email to verify your account";
            public const string UPDATE_USER = "User updated Successfully";
            public const string EMAIL_NOT_SENT = "Couldn't Send Verification Email, Try Again Later!";
            public const string NO_EMAIL = "No email provided";
            public const string NO_PASSWORD = "No password provided";
            public const string NOT_FOUND = "{0} not found.";
            public const string USER_NOT_FOUND = "User not found.";
            public const string LOGIN_FAILED = "Please enter valid username / password";
            public const string NOT_VERIFIED = "User account not verified";
            public const string VERIFIED = "User account verified successfully";
            public const string RESET_PASS_EMAIL = "Check your email to reset your password";
            public const string INVALID_TOKEN = "Invalid token supplied";
            public const string PASSWORD_UPDATED = "Password changed successfully";
            public const string VERIFY_ACCOUNT = "Check your email to verify your account";
            public const string INCORRECT_PASSWORD = "Current password is incorrect";
            public const string SAME_PASSWORD = "New password is same as old password";
            public const string PUSH_TOKEN_UPDATE = "Push token updated successfully";


            //Vendor
            public const string VENDOR_EXIST = "Vendor already Exist.";
            public const string VENDOR_ADDED = "Vendor added successfully";
            public const string VENDOR_NOT_FOUND = "Vendor not found.";
            public const string UPDATE_VENDOR = "Vendor updated Successfully";
            public const string DELETE_VENDOR = "Vendor deleted Successfully";

            //Customer
            public const string CUSTOMER_EXIST = "Customer already Exist.";
            public const string CUSTOMER_ADDED = "Customer added successfully";
            public const string CUSTOMER_NOT_FOUND = "Customer not found.";
            public const string UPDATE_CUSTOMER = "Customer updated Successfully";
            public const string DELETE_CUSTOMER = "Customer deleted Successfully";

            //Staff
            public const string STAFF_EXIST = "Staff already Exist.";
            public const string STAFF_ADDED = "Staff added successfully";
            public const string STAFF_NOT_FOUND = "Staff not found.";
            public const string UPDATE_STAFF = "Staff updated Successfully";
            public const string DELETE_STAFF= "Staff deleted Successfully";

            //Product
            public const string PRODUCT_EXIST = "Product already Exist.";
            public const string PRODUCT_ADDED = "Product added successfully";
            public const string PRODUCT_NOT_FOUND = "Product not found.";
            public const string UPDATE_PRODUCT = "Product updated Successfully";
            public const string DELETE_PRODUCT = "Product deleted Successfully";


        }


        public static class AppConfig
        {
            public static string EMAIL = "dev.techelix@gmail.com";
            public static string EMAIL_NAME = "Techelix";
        }
    }
}
