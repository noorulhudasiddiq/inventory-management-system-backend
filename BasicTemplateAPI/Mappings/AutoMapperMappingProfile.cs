﻿using AutoMapper;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryManagementAPI.Api.Mappings
{
    public class AutoMapperMappingProfile : Profile
    {
        public AutoMapperMappingProfile()
        {
            this.CreateMap<User, LoginResDTO>();
            this.CreateMap<RegisterReqDTO, User>();
            this.CreateMap<Vendor, VendorResDTO>();
            this.CreateMap<VendorResDTO, Vendor>();
        }
    }
}
