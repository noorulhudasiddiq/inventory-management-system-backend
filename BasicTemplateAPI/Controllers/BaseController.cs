﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using InventoryManagementAPI.Api.Utils;
using Recipe.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.Common.Helper;
using Spark.Common.Helper;

namespace InventoryManagementAPI.Api.Controllers
{
    [Produces("application/json")]
    public class BaseController : Controller
    {
        protected virtual dynamic ThrowValidationError(Exception ex)
        {
            return new ValidationFailedResult(ex);
        }

        protected virtual dynamic JsonResponse(dynamic obj)
        {
            if (obj.HasErrors)
            {
                return ThrowValidationError(obj.Error);
            }
            else
            {
                return this.Ok(obj);
            }
        }

        protected virtual dynamic JsonResponse<TDTO, TEntity>(DataTransferObject<TEntity> entityObject)
            where TDTO : new()
        {
            if (entityObject.HasErrors)
            {
                return new ValidationFailedResult(entityObject.Error);
            }
            else
            {

                DataTransferObject<TDTO> dtoObject = new DataTransferObject<TDTO>();

                TDTO instance = new TDTO();

                ObjectHelper.CopyObject(entityObject.Result, ref instance);


                return this.Ok(dtoObject);
            }
        }

        protected int GetUserId()
        {
            return int.Parse(this.User.Claims.First(i => i.Type == "userID").Value);
        }

        
    }
}