﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagementAPI.Api.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementAPI.Controllers
{
    [Route("api/customer")]
    [ApiController]
    public class CustomerController : BaseController
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }


        [HttpPost()]
        [ProducesResponseType(typeof(DataTransferObject<CustomerResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<CustomerResDTO>))]
        public async Task<IActionResult> AddCustomer([FromBody]AddCustomerReqDTO dtoObj)
        {
            return this.JsonResponse(await _customerService.AddCustomer(dtoObj));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(DataTransferObject<CustomerResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<CustomerResDTO>))]
        public async Task<IActionResult> GetCustomer([FromQuery]int customerID)
        {
            return this.JsonResponse(await _customerService.GetCustomer(customerID));
        }

        [HttpGet()]
        [ProducesResponseType(typeof(DataTransferObject<List<CustomerResDTO>>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<List<CustomerResDTO>>))]
        public async Task<IActionResult> GetCustomers()
        {
            return this.JsonResponse(await _customerService.GetAllCustomer());
        }

        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(DataTransferObject<CustomerResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<CustomerResDTO>))]
        public async Task<IActionResult> UpdateCustomer([FromQuery] int customerID, [FromBody] UpdateCustomerReqDTO dtoObj)
        {
            return this.JsonResponse(await _customerService.UpdateCustomer(customerID, dtoObj));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(DataTransferObject<CustomerResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<CustomerResDTO>))]
        public async Task<IActionResult> DeleteCustomer([FromQuery] int customerID)
        {
            return this.JsonResponse(await _customerService.DeleteCustomer(customerID));
        }
    }
}