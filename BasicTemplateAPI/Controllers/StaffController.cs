﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagementAPI.Api.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StaffController : BaseController
    {
        private readonly IStaffService _staffService;

        public StaffController(IStaffService staffService)
        {
            _staffService = staffService;
        }


        [HttpPost()]
        [ProducesResponseType(typeof(DataTransferObject<StaffResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<StaffResDTO>))]
        public async Task<IActionResult> AddStaff([FromBody]AddStaffReqDTO dtoObj)
        {
            return this.JsonResponse(await _staffService.AddStaff(dtoObj));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(DataTransferObject<StaffResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<StaffResDTO>))]
        public async Task<IActionResult> GetStaff([FromQuery]int staffID)
        {
            return this.JsonResponse(await _staffService.GetStaff(staffID));
        }

        [HttpGet()]
        [ProducesResponseType(typeof(DataTransferObject<List<StaffResDTO>>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<List<StaffResDTO>>))]
        public async Task<IActionResult> GetStaffs()
        {
            return this.JsonResponse(await _staffService.GetAllStaff());
        }

        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(DataTransferObject<StaffResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<StaffResDTO>))]
        public async Task<IActionResult> UpdateStaff([FromQuery] int staffID, [FromBody] UpdateStaffReqDTO dtoObj)
        {
            return this.JsonResponse(await _staffService.UpdateStaff(staffID, dtoObj));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(DataTransferObject<StaffResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<StaffResDTO>))]
        public async Task<IActionResult> DeleteStaff([FromQuery] int staffID)
        {
            return this.JsonResponse(await _staffService.DeleteStaff(staffID));
        }
    }
}