﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagementAPI.Api.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : BaseController
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }


        [HttpPost()]
        [ProducesResponseType(typeof(DataTransferObject<ProductResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<ProductResDTO>))]
        public async Task<IActionResult> AddStaff([FromBody]AddProductReqDTO dtoObj)
        {
            return this.JsonResponse(await _productService.AddProduct(dtoObj));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(DataTransferObject<ProductResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<ProductResDTO>))]
        public async Task<IActionResult> GetStaff([FromQuery]int productID)
        {
            return this.JsonResponse(await _productService.GetProduct(productID));
        }

        [HttpGet()]
        [ProducesResponseType(typeof(DataTransferObject<List<ProductResDTO>>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<List<ProductResDTO>>))]
        public async Task<IActionResult> GetStaffs()
        {
            return this.JsonResponse(await _productService.GetAllProduct());
        }

        //[HttpPatch("{id}")]
        //[ProducesResponseType(typeof(DataTransferObject<ProductResDTO>), 200)]
        //[Produces("application/json", Type = typeof(DataTransferObject<ProductResDTO>))]
        //public async Task<IActionResult> UpdateStaff([FromQuery] int productID, [FromBody] UpdateStaffReqDTO dtoObj)
        //{
        //    return this.JsonResponse(await _productService.UpdateProduct(productID, dtoObj));
        //}

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(DataTransferObject<ProductResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<ProductResDTO>))]
        public async Task<IActionResult> DeleteStaff([FromQuery] int productID)
        {
            return this.JsonResponse(await _productService.DeleteProduct(productID));
        }
    }
}