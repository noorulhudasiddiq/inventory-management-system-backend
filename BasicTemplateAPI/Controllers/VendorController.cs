﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagementAPI.Api.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementAPI.Controllers
{
    [Route("api/vendor")]
    [ApiController]
    public class VendorController : BaseController
    {
        private readonly IVendorService _vendorService;

        public VendorController(IVendorService vendorService)
        {
            _vendorService = vendorService;
        }


        [HttpPost()]
        [ProducesResponseType(typeof(DataTransferObject<VendorResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<VendorResDTO>))]
        public async Task<IActionResult> AddVendor([FromBody]AddVendorReqDTO dtoObj)
        {
            return this.JsonResponse(await _vendorService.AddVendor(dtoObj));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(DataTransferObject<VendorResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<VendorResDTO>))]
        public async Task<IActionResult> GetVendor([FromQuery]int vendorID)
        {
            return this.JsonResponse(await _vendorService.GetVendor(vendorID));
        }

        [HttpGet()]
        [ProducesResponseType(typeof(DataTransferObject<List<VendorResDTO>>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<List<VendorResDTO>>))]
        public async Task<IActionResult> GetVendors()
        {
            return this.JsonResponse(await _vendorService.GetAllVendor());
        }

        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(DataTransferObject<VendorResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<VendorResDTO>))]
        public async Task<IActionResult> UpdateVendor([FromQuery] int vendorID , [FromBody] UpdateVendorReqDTO dtoObj)
        {
            return this.JsonResponse(await _vendorService.UpdateVendor(vendorID, dtoObj));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(DataTransferObject<VendorResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<VendorResDTO>))]
        public async Task<IActionResult> DeleteVendor([FromQuery] int vendorID)
        {
            return this.JsonResponse(await _vendorService.DeleteVendor(vendorID));
        }
    }
}