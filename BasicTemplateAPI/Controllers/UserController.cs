﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagementAPI.Api.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace InventoryManagementAPI.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPatch("changePassword")]
        [ProducesResponseType(typeof(DataTransferObject<LoginResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<LoginResDTO>))]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordReqDTO dtoObj)
        {
            int userID = GetUserId();
            return this.JsonResponse(await _userService.ChangePassword(userID, dtoObj));
        }

        [HttpGet()]
        [ProducesResponseType(typeof(DataTransferObject<LoginResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<LoginResDTO>))]
        public async Task<IActionResult> GetUser()
        {
            int userID = GetUserId();
            return this.JsonResponse(await _userService.GetUser(userID));
        }

        [HttpPatch("update")]
        [ProducesResponseType(typeof(DataTransferObject<LoginResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<LoginResDTO>))]
        public async Task<IActionResult> UpdateUser([FromBody]UpdateUserReqDTO dtoObj)
        {
            int userID = GetUserId();
            return this.JsonResponse(await _userService.UpdateUser(userID, dtoObj));
        }

        [HttpPatch("token/{token}")]
        [ProducesResponseType(typeof(DataTransferObject<LoginResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<LoginResDTO>))]
        public async Task<IActionResult> UpdateUser([FromRoute] string token)
        {
            int userID = GetUserId();
            return this.JsonResponse(await _userService.UpdatePushToken(userID, token));
        }
    }
}