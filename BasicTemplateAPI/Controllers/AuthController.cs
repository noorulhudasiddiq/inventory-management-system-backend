﻿using System.Threading.Tasks;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Recipe.NetCore.Base.Abstract;

namespace InventoryManagementAPI.Api.Controllers
{
    [AllowAnonymous]
    [Route("api/auth")]
    public class AuthController : BaseController
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("register")]
        [ProducesResponseType(typeof(DataTransferObject<dynamic>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<dynamic>))]
        public async Task<IActionResult> Register([FromBody] RegisterReqDTO dtoObj)
        {
           
            return this.JsonResponse(await _authService.Register(dtoObj));
        }

        [HttpPost("login")]
        [ProducesResponseType(typeof(DataTransferObject<LoginResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<LoginResDTO>))]
        public async Task<IActionResult> Login([FromBody] LoginReqDTO dtoObj)
        {
            return this.JsonResponse(await _authService.Login(dtoObj));
        }

        [HttpPost("verify")]
        [ProducesResponseType(typeof(DataTransferObject<LoginResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<LoginResDTO>))]
        public async Task<IActionResult> VerifyUser([FromBody] VerifyUserReqDTO dtoObj)
        {
            return this.JsonResponse(await _authService.VerifyUser(dtoObj));
        }

        [HttpPatch("forgetPassword")]
        [ProducesResponseType(typeof(DataTransferObject<bool>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<bool>))]
        public async Task<IActionResult> ForgetPassword([FromBody]string email)
        {
            return this.JsonResponse(await _authService.ForgetPassword(email));
        }


        [HttpPatch("verifyForgetPassword")]
        [ProducesResponseType(typeof(DataTransferObject<LoginResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<LoginResDTO>))]
        public async Task<IActionResult> VerifyForgetPassword([FromBody] VerifyForgetPasswordReqDTO dtoObj)
        {
            return this.JsonResponse(await _authService.VerifyForgetPassword(dtoObj));
        }

        [HttpPost("resendVerificationEmail")]
        [ProducesResponseType(typeof(DataTransferObject<LoginResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<LoginResDTO>))]
        public async Task<IActionResult> ResendVerificationEmail([FromBody]string email)
        {
            return this.JsonResponse(await _authService.ResendVerificationEmail(email));
        }



        [HttpPost("refresh")]
        [ProducesResponseType(typeof(DataTransferObject<TokenResDTO>), 200)]
        [Produces("application/json", Type = typeof(DataTransferObject<TokenResDTO>))]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshReqDTO tokenObj)
        {
            return this.JsonResponse(await _authService.Refresh(tokenObj));
        }


    }

}