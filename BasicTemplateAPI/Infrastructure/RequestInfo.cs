﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using InventoryManagement.Core.Models;
using InventoryManagement.NetCore.Base.Interface;

namespace InventoryManagementAPI.Api.Infrastructure
{
        public class RequestInfo : IRequestInfo
        {
            private readonly InventoryManagementContext dbContext;
           
            public RequestInfo(InventoryManagementContext context)
            {
                dbContext = context;
             
            }

         DbContext IRequestInfo.Context => dbContext;
    }
}
