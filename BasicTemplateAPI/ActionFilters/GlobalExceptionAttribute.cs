﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using InventoryManagement.Core.Models;

namespace InventoryManagementAPI.Api.ActionFilters
{
    public class GlobalExceptionAttribute : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            int status = 500;
            var descriptor = context.ActionDescriptor as ControllerActionDescriptor;
            var actionName = descriptor.ActionName;
            var ctrlName = descriptor.ControllerName;

            var exception = new Exception(context.Exception.Message + string.Format(":  Controller: {0}  Action: {1}", ctrlName, actionName), context.Exception);

            //TelemetryClient telemetryClient = new TelemetryClient();
            //telemetryClient.TrackException(exception);

            var result = new ObjectResult(new ValidationResultModel(context));
            result.StatusCode = status;
            context.Result = result;

        }
    }
}
