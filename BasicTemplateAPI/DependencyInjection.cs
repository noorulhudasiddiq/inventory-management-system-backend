﻿using InventoryManagement.Common.Configuration;
using InventoryManagement.Core.IRepository;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Interface;
using InventoryManagement.NetCore.Base.Generic;
using InventoryManagement.Services;
using InventoryManagementAPI.Api.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using InventoryManagement.Repository;


namespace InventoryManagement.Api
{
    public class DependencyInjection
    {
        public void Map(IServiceCollection services, IConfiguration Configuration)
        {
            #region Application

            //#region Configuration
            //services.Configure<RefreshTokenConfiguration>(Configuration.GetSection("RefreshToken"));
            services.Configure<EmailConfiguration>(Configuration.GetSection("EmailConfiguration"));
            //services.Configure<AzureBlobConfiguration>(Configuration.GetSection("AzureBlobConfiguration"));
            //services.Configure<WebUIConfiguration>(Configuration.GetSection("WebUIConfiguration"));
            //services.Configure<FirebaseServiceAccountConfigurations>(Configuration.GetSection("FirebaseServiceAccountConfigurations"));
            //services.Configure<FirebaseServerConfigurations>(Configuration.GetSection("FirebaseServerConfigurations"));
            //services.Configure<FirebaseWebConfigurations>(Configuration.GetSection("FirebaseWebConfigurations"));

            #endregion

            #region Base
            services.AddHttpContextAccessor();
            services.AddScoped<IRequestInfo, RequestInfo>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IService), typeof(Service));
            #endregion

            //#region Email
            //services.AddScoped(typeof(IEmailService), typeof(EmailService));
            //#endregion

            //#region Firebase
            //services.AddScoped<IFirebaseClientHelper, FirebaseClientHelper>();
            //#endregion



            //#region Auth
            //services.AddScoped<IJwtFactory, JwtFactory>();

            services.AddScoped(typeof(IUserRepository), typeof(UserRepository));
            services.AddScoped(typeof(IRefreshTokenRepository), typeof(RefreshTokenRepository));
            services.AddScoped(typeof(IProductRepository), typeof(ProductRepository));
            services.AddScoped(typeof(ICustomerRepository), typeof(CustomerRepository));
            services.AddScoped(typeof(IVendorRepository), typeof(VendorRepository));
            services.AddScoped(typeof(IStaffRepository), typeof(StaffRepository));



            services.AddScoped(typeof(IAuthService), typeof(AuthService));
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(IVendorService), typeof(VendorService));
            services.AddScoped(typeof(IProductService), typeof(ProductService));
            services.AddScoped(typeof(IStaffService), typeof(StaffService));
            services.AddScoped(typeof(ICustomerService), typeof(CustomerService));
            //services.AddScoped(typeof(INotificationService), typeof(NotificationService));

            //services.AddScoped<IPasswordHasher<ApplicationUser>, PasswordHasher<ApplicationUser>>();
            //#endregion            

            // #endregion

        }
    }
}
