﻿using AutoMapper;
using InventoryManagement.Common;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Generic;
using InventoryManagement.NetCore.Base.Interface;
using Spark.Common.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Services
{
    public class UserService : Service<IUserRepository, User, int>, IUserService
    {
        private readonly IUserRepository _repository;
        private readonly IMapper _mapper;
        private readonly IRefreshTokenRepository _refreshTokenRepository;


        public UserService(IUnitOfWork unitOfWork
            , IUserRepository repository
            , IMapper mapper
            , IRefreshTokenRepository refreshTokenRepository

            ) : base(unitOfWork, repository, mapper)
        {
            _mapper = mapper;

            _repository = repository;
            _refreshTokenRepository = refreshTokenRepository;

        }

        public async Task<DataTransferObject<bool>> ChangePassword(int userID, ChangePasswordReqDTO dtoObj)
        {
            var result = new DataTransferObject<bool>();

            var userDetails = FindUserByID(userID).Result;
            if (userDetails == null)
            {
                result = new DataTransferObject<bool>(false, 2, Constant.ResponseMessage.USER_NOT_FOUND);
                return result;
            }

            //IncorrectPassword
            if (!HashPasswordHelper.CheckMatch256(userDetails.Password, dtoObj.CurrentPassword))
            {
                result = new DataTransferObject<bool>(false, 3, Constant.ResponseMessage.INCORRECT_PASSWORD);
                return result;
            }

            //SamePassword
            if (HashPasswordHelper.CheckMatch256(userDetails.Password, dtoObj.NewPassword))
            {
                result = new DataTransferObject<bool>(false, 3, Constant.ResponseMessage.SAME_PASSWORD);
                return result;
            }

            userDetails.Password = HashPasswordHelper.CalculateHash256(dtoObj.NewPassword);

            await this.UpdateEntityAsync(userDetails);

            result = new DataTransferObject<bool>(false, 2, Constant.ResponseMessage.PASSWORD_UPDATED);
            return result;
        }


        public async Task<DataTransferObject<LoginResDTO>> GetUser(int userID)
        {
            var userDetails = FindUserByID(userID).Result;

            var dtoRes = _mapper.Map<LoginResDTO>(userDetails);
            var result = new DataTransferObject<LoginResDTO>(dtoRes);

            return result;
        }

        public async Task<DataTransferObject<LoginResDTO>> UpdateUser(int userID, UpdateUserReqDTO dtoObj)
        {
            var result = new DataTransferObject<LoginResDTO>();

            var userDetails = FindUserByID(userID).Result;
            if (userDetails == null)
            {
                result = new DataTransferObject<LoginResDTO>(false, 2, Constant.ResponseMessage.USER_NOT_FOUND);
                return result;
            }

            userDetails.Name = dtoObj.Name;
            userDetails.ImageUrl = dtoObj.ImageUrl;

            await this.UpdateEntityAsync(userDetails);

            var dtoRes = _mapper.Map<LoginResDTO>(userDetails);

            result = new DataTransferObject<LoginResDTO>(dtoRes, 1, Constant.ResponseMessage.UPDATE_USER);
            return result;
        }

        public async Task<DataTransferObject<bool>> UpdatePushToken(int userID, string pushToken)
        {
           

            var userDetails = FindUserByID(userID).Result;
            if (userDetails == null)
            {
                return new DataTransferObject<bool>(true, 2, Constant.ResponseMessage.USER_NOT_FOUND);
                
            }

            userDetails.PushToken = pushToken;
            await this.UpdateEntityAsync(userDetails);

            return new DataTransferObject<bool>(1, Constant.ResponseMessage.PUSH_TOKEN_UPDATE);

        }


        public async Task<User> FindUserByEmail(string email)
        {
            User userDetails = (await _repository.Find(x => x.Email == email)); ;

            return userDetails;
        }


        public async Task<User> FindUserByID(int ID)
        {
            User userDetails = (await _repository.Find(x => x.ID == ID)); ;

            return userDetails;
        }
    }
}
