﻿
using System;
using System.Threading.Tasks;
using AutoMapper;
using InventoryManagement.NetCore.Base.Generic;
using InventoryManagement.Core.IRepository;
using InventoryManagement.Core.Entity;
using InventoryManagement.NetCore.Base.Interface;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.Core.DTO.Request;
using Spark.Common.Helper;
using InventoryManagement.Core.IService;
using InventoryManagement.Common.Helper;
using System.Security.Claims;
using System.Collections.Generic;
using InventoryManagement.Core.DTO.Response;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Cryptography;
using InventoryManagement.Common;
using InventoryManagement.Common.Configuration;

namespace InventoryManagement.Services
{
    public class AuthService : Service<IUserRepository, User, int>, IAuthService
    {
        private readonly IUserRepository _repository;
        private readonly IMapper _mapper;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IUserService _userService;


        public AuthService(IUnitOfWork unitOfWork
            , IUserRepository repository
            , IMapper mapper
            , IRefreshTokenRepository refreshTokenRepository
            , IUserService userService

            ) : base(unitOfWork, repository, mapper)
        {
            _mapper = mapper;
            
            _repository = repository;
            _refreshTokenRepository = refreshTokenRepository;
            _userService = userService;
           
        }


        public async Task<DataTransferObject<bool>> Register( RegisterReqDTO dtoObj)
        {

            var result = new DataTransferObject<bool>();

            var foundObj = _userService.FindUserByEmail(dtoObj.Email).Result;

            if (foundObj != null)
            {
                result = new DataTransferObject<bool>(true, 0, Constant.ResponseMessage.USER_EXIST);
                return result;
            }

            //Creating random token
            Random rnd = new Random();
            var token = rnd.Next(100000, 999999).ToString();

            //Mapping User Object 
            var userObj = _mapper.Map<User>(dtoObj);
            userObj.Password = HashPasswordHelper.CalculateHash256(userObj.Password);
            userObj.TokenActivate = token;
            userObj.Status = true;
            userObj.EmailVerified = false;
            var user = await  _repository.Create(userObj);

            var subject = "Verify your account";
            var body = "Thanks for registering. your code is " + token;


            if (EmailConfiguration.SendEmail(subject, body,userObj.Email))
            {

                // save changes only when the email has been sent.
                var entriesCommited = _unitOfWork.SaveAsync();

                if (entriesCommited.Result > 0)
                {
                    result = new DataTransferObject<bool>(1, Constant.ResponseMessage.USER_REGISTERED);
                    return result;
                }
                
            }
            else
            {
                 result = new DataTransferObject<bool>(true, 1, Constant.ResponseMessage.EMAIL_NOT_SENT);
                return result;
            }

            return result;
        }


        public async Task<DataTransferObject<LoginResDTO>> Login(LoginReqDTO dtoObj)
        {
            var result = new DataTransferObject<LoginResDTO>();

            if (dtoObj.Email != null && dtoObj.Email.Length == 0)
            {
                result = new DataTransferObject<LoginResDTO>(true, 0, Constant.ResponseMessage.NO_EMAIL);

                return result;
            }

            if (dtoObj.Password != null && dtoObj.Password.Length == 0)
            {
                result = new DataTransferObject<LoginResDTO>(true, 0, Constant.ResponseMessage.NO_PASSWORD);

                return result;
            }

            var userDetails = _userService.FindUserByEmail(dtoObj.Email).Result;
            if (userDetails == null)
            {
                result = new DataTransferObject<LoginResDTO>(false, 2, Constant.ResponseMessage.USER_NOT_FOUND);
                return result;
            }


            else
            {
                if (!userDetails.EmailVerified)
                {
                    result = new DataTransferObject<LoginResDTO>(false, 3, Constant.ResponseMessage.NOT_VERIFIED);
                    return result;
                }

                if (!HashPasswordHelper.CheckMatch256(userDetails.Password, dtoObj.Password))
                {
                    result = new DataTransferObject<LoginResDTO>(false, 3, Constant.ResponseMessage.LOGIN_FAILED);
                    return result;
                }

                userDetails.PushToken = dtoObj.PushToken;
                await this.UpdateEntityAsync(userDetails);

                var userDetailsObj = _mapper.Map<LoginResDTO>(userDetails);

                

                userDetailsObj.Token = new TokenResDTO
                {
                    AccessToken = GenerateToken(CreateClaims(userDetails.ID), 30),
                    AccessTokenExpiration = DateTime.UtcNow.AddDays(30),
                    RefreshToken = await GenerateRefreshToken(userDetails.ID)
                };

                result = new DataTransferObject<LoginResDTO>(userDetailsObj);
            }

            return result; 
        }



        public async Task<DataTransferObject<LoginResDTO>> VerifyUser(VerifyUserReqDTO dtoObj)
        {
            var result = new DataTransferObject<LoginResDTO>();

            var userDetails = _userService.FindUserByEmail(dtoObj.Email).Result;
            if(userDetails == null)
            {
                result = new DataTransferObject<LoginResDTO>(false, 2, Constant.ResponseMessage.USER_NOT_FOUND);
                return result;
            }
                

            if (dtoObj.Token == userDetails.TokenActivate)
            {
                userDetails.EmailVerified = true;
                userDetails = await this.UpdateEntityAsync(userDetails);
            }


            var userDetailsObj = _mapper.Map<LoginResDTO>(userDetails);

            result = new DataTransferObject<LoginResDTO>(userDetailsObj, 1, Constant.ResponseMessage.VERIFIED);
            return result;
        }



        public async Task<DataTransferObject<bool>> ForgetPassword(string email)
        {
            var result = new DataTransferObject<bool>();

            var userDetails = _userService.FindUserByEmail(email).Result;
            if (userDetails == null)
            {
                result = new DataTransferObject<bool>(false, 2, Constant.ResponseMessage.USER_NOT_FOUND);
                return result;
            }

            
                if (!userDetails.EmailVerified)
                {
                    result = new DataTransferObject<bool>(false, 3, Constant.ResponseMessage.NOT_VERIFIED);
                    return result;
                }

                Random rnd = new Random();
                userDetails.TokenForget = rnd.Next(100000, 999999).ToString();

                await _repository.Update(userDetails);

                var subject  = "Reset your password token";
                var body = "Your code is " + userDetails.TokenForget;

           


            if (EmailConfiguration.SendEmail(subject, body, userDetails.Email))
            {

                    // save changes only when the email has been sent.
                    var entriesCommited = _unitOfWork.SaveAsync();

                    if (entriesCommited.Result > 0)
                    {
                        result = new DataTransferObject<bool>(1, Constant.ResponseMessage.RESET_PASS_EMAIL);
                        return result;
                    }

                }
                else
                {
                    result = new DataTransferObject<bool>(true, 1, Constant.ResponseMessage.EMAIL_NOT_SENT);
                    return result;
                }

                return result;
            
        }



        public async Task<DataTransferObject<bool>> VerifyForgetPassword(VerifyForgetPasswordReqDTO dtoObj)
        {
            var result = new DataTransferObject<bool>();

            var userDetails = _userService.FindUserByEmail(dtoObj.Email).Result;
            if (userDetails == null)
            {
                result = new DataTransferObject<bool>(false, 2, Constant.ResponseMessage.USER_NOT_FOUND);
                return result;
            }

            if(userDetails.TokenForget != dtoObj.Token)
            {
                result = new DataTransferObject<bool>(false, 2, Constant.ResponseMessage.INVALID_TOKEN);
                return result;
            }

            userDetails.Password = HashPasswordHelper.CalculateHash256(dtoObj.Password);

            await this.UpdateEntityAsync(userDetails);

            result = new DataTransferObject<bool>(false, 2, Constant.ResponseMessage.PASSWORD_UPDATED);
            return result;

        }



        public async Task<DataTransferObject<bool>> ResendVerificationEmail(string email)
        {
            var result = new DataTransferObject<bool>();

            var userDetails = _userService.FindUserByEmail(email).Result;
            if (userDetails == null)
            {
                result = new DataTransferObject<bool>(false, 2, Constant.ResponseMessage.USER_NOT_FOUND);
                return result;
            }


            Random rnd = new Random();
            userDetails.TokenActivate = rnd.Next(100000, 999999).ToString();

            await _repository.Update(userDetails);

            var subject = "Verify your account";
            var body = "Thanks for registering. your code is " + userDetails.TokenActivate;

            if (EmailConfiguration.SendEmail(subject, body, userDetails.Email))
            {

                // save changes only when the email has been sent.
                var entriesCommited = _unitOfWork.SaveAsync();

                if (entriesCommited.Result > 0)
                {
                    result = new DataTransferObject<bool>(1, Constant.ResponseMessage.VERIFY_ACCOUNT);
                    return result;
                }

            }
            else
            {
                result = new DataTransferObject<bool>(true, 1, Constant.ResponseMessage.EMAIL_NOT_SENT);
                return result;
            }

            return result;

        }

            private List<Claim> CreateClaims(int id)
        {
            var claims = new List<Claim>();

            claims.Add(new Claim("userID", id.ToString()));

            return claims;
        }


        public string GenerateToken(IEnumerable<Claim> claims, double expirationInDays)
        {

            string SecretKey = Environment.GetEnvironmentVariable("TokenSecretKey");
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(SecretKey);

            var claimsDictionary = new Dictionary<string, object>();

            foreach (var claim in claims)
            {
                claimsDictionary.Add(claim.Type, claim.Value);
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Claims = claimsDictionary,
                Expires = DateTime.UtcNow.AddDays(expirationInDays),
                SigningCredentials =
                    new SigningCredentials(
                        new SymmetricSecurityKey(key),
                        SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }


        public async Task<string> GenerateRefreshToken(int userID)
        {
            // Create the refresh token

            string refreshToken = null;

            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                refreshToken = Convert.ToBase64String(randomNumber);
            }

            RefreshToken token = new RefreshToken()
            {
                UserID = userID,
                Token = refreshToken,
                Expiry = DateTime.UtcNow.AddDays(360)

            };
            await _refreshTokenRepository.Create(token);
            await _unitOfWork.SaveAsync();

            return refreshToken;
        }


        public async Task<DataTransferObject<TokenResDTO>> Refresh(RefreshReqDTO tokenObj)
        {
            TokenResDTO res = null;
            var userID = GetUserIdFromAccessToken(tokenObj.AccessToken);

            if (await ValidateRefreshTokenAsync(userID, tokenObj.RefreshToken))
            {

                var userDetails = (await _repository.Find(x => x.ID == userID));

                res = new TokenResDTO
                {
                    AccessToken = GenerateToken(CreateClaims(userDetails.ID), 600),
                    AccessTokenExpiration = DateTime.UtcNow.AddDays(30),
                    RefreshToken = tokenObj.RefreshToken
                };

            }


            if (res == null)
            {
                return new DataTransferObject<TokenResDTO>(true, "Invalid or Expired Refresh Token");
            }
            else
            {
                return new DataTransferObject<TokenResDTO>(res);
            }
        }


        private long GetUserIdFromAccessToken(string accessToken)
        {
            string SecretKey = Environment.GetEnvironmentVariable("TokenSecretKey");
            var tokenValidationParamters = new TokenValidationParameters
            {
                ValidateAudience = false, // You might need to validate this one depending on your case
                ValidateIssuer = false,
                ValidateActor = false,
                ValidateLifetime = false, // Do not validate lifetime here
                IssuerSigningKey =
                    new SymmetricSecurityKey(
                        Encoding.ASCII.GetBytes(SecretKey)
                    )
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(accessToken, tokenValidationParamters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            {
                
                return 0;
            }


            var userId = principal.FindFirst("UserID")?.Value;
            

            if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(userId))
            {
                return long.Parse(userId);
            }

            return 0;
        }



        public async Task<bool> ValidateRefreshTokenAsync(long userID, string refreshToken)
        {

            var token = (await _refreshTokenRepository.Find(rt => rt.UserID == userID && rt.Token.Equals(refreshToken)));
            if (token == null)
            {
                return false;
            }

            // Ensure that the refresh token that we got from storage is not yet expired.
            if (DateTime.UtcNow > token.Expiry)
            {
                await _refreshTokenRepository.DeleteAsync(token.ID);
                _unitOfWork.DBContext.SaveChanges();
                return false;
            }

            return true;
        }


    }
}
