﻿using InventoryManagement.Common.DTO;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using FirebaseAdmin.Messaging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Services
{
    public class FirebaseNotificationService : IFirebaseNotificationService
    {
        public async Task<TopicManagementResponse> SubsribeToTopic(FirebaseTopicSubUnsubDTO dtoObj)
        {
            var response = await FirebaseMessaging.DefaultInstance.SubscribeToTopicAsync(dtoObj.pushTokens, dtoObj.Topic);
            return response;
        }

        public async Task<TopicManagementResponse> UnSubsribeToTopic(FirebaseTopicSubUnsubDTO dtoObj)
        {
            var response = await FirebaseMessaging.DefaultInstance.UnsubscribeFromTopicAsync(dtoObj.pushTokens, dtoObj.Topic);
            return response;
        }

        public async Task<string> SendNotificationToSpecificDevice(FirebaseNotificationToSpecificDeviceDTO dtoObj)
        {

            var message = new Message()
            {
                Notification = new Notification()
                {
                    Title = dtoObj.Title,
                    Body = dtoObj.Body

                },
                Token = dtoObj.PushToken
            };
            var response = await FirebaseMessaging.DefaultInstance.SendAsync(message);

            return response;
        }

        public async Task<BatchResponse> SendNotificationToDevices(FirebaseNotificationToDevicesDTO dtoObj)
        {

            var message = new MulticastMessage()
            {
                Notification = new Notification()
                {
                    Title =  dtoObj.Title ,
                    Body =  dtoObj.Body

                },
                Tokens = dtoObj.PushTokens
            };
            var response = await FirebaseMessaging.DefaultInstance.SendMulticastAsync(message);

            return response;
        }

        public async Task<string> SendNotificationToTopic(FirebaseNotificationToTopicDTO dtoObj)
        {
            var message = new Message()
            {
                Notification = new Notification()
                {
                    Title = dtoObj.Title,
                    Body = dtoObj.Body

                },
                Topic = dtoObj.Topic
            };
            var response = await FirebaseMessaging.DefaultInstance.SendAsync(message);

            return response;
        }

        public async Task<BatchResponse> SendBatchOfNotificationsToTopic(List<FirebaseNotificationToTopicDTO> dtoObj)
        {
            var messages = new List<Message>();
            foreach (var obj in dtoObj)
            {
                var msg = new Message()
                {
                    Notification = new Notification()
                    {
                        Title = obj.Title,
                        Body = obj.Body

                    },
                    Topic = obj.Topic
                };
                messages.Add(msg);

            }
           
            
            var response = await FirebaseMessaging.DefaultInstance.SendAllAsync(messages);

            return response;
        }

    }
}
