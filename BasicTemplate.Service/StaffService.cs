﻿using AutoMapper;
using InventoryManagement.Common;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Generic;
using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Services
{
    public class StaffService : Service<IStaffRepository, Staff, int>, IStaffService
    {
        private readonly IStaffRepository _repository;
        private readonly IMapper _mapper;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IUserService _userService;


        public StaffService(IUnitOfWork unitOfWork
            , IStaffRepository repository
            , IMapper mapper
            , IRefreshTokenRepository refreshTokenRepository
            , IUserService userService

            ) : base(unitOfWork, repository, mapper)
        {
            _mapper = mapper;

            _repository = repository;
            _refreshTokenRepository = refreshTokenRepository;
            _userService = userService;

        }

        public async Task<DataTransferObject<StaffResDTO>> AddStaff(AddStaffReqDTO dtoObj)
        {
            var result = new DataTransferObject<StaffResDTO>();

            var foundStaff = await _repository.Find(v => v.Phone == dtoObj.Phone);
            if (foundStaff != null)
            {
                result = new DataTransferObject<StaffResDTO>(true, 0, Constant.ResponseMessage.STAFF_EXIST);
                return result;
            }

            var staffObj = _mapper.Map<Staff>(dtoObj);
            staffObj.isActive = true;
            await _repository.Create(staffObj);


            var resObj = _mapper.Map<StaffResDTO>(staffObj);

            result = new DataTransferObject<StaffResDTO>(resObj, 1, Constant.ResponseMessage.STAFF_ADDED);
            return result;

        }

        public async Task<DataTransferObject<StaffResDTO>> GetStaff(int staffID)
        {

            var result = new DataTransferObject<StaffResDTO>();

            var foundStaff = await _repository.Find(v => v.ID == staffID && v.isActive);

            if (foundStaff == null)
            {
                result = new DataTransferObject<StaffResDTO>(true, 0, Constant.ResponseMessage.STAFF_NOT_FOUND);
                return result;
            }

            var resObj = _mapper.Map<StaffResDTO>(foundStaff);
            result = new DataTransferObject<StaffResDTO>(resObj);
            return result;
        }

        public async Task<DataTransferObject<List<StaffResDTO>>> GetAllStaff()
        {

            var result = new DataTransferObject<List<StaffResDTO>>();

            var foundStaffs = await _repository.FindAll(v => v.isActive);

            if (foundStaffs == null)
            {
                result = new DataTransferObject<List<StaffResDTO>>(true, 0, Constant.ResponseMessage.STAFF_NOT_FOUND);
                return result;
            }

            List<StaffResDTO> staffList = new List<StaffResDTO>();
            foreach (var staff in foundStaffs)
            {
                staffList.Add(_mapper.Map<StaffResDTO>(staff));
            }


            result = new DataTransferObject<List<StaffResDTO>>(staffList);
            return result;
        }

        public async Task<DataTransferObject<StaffResDTO>> UpdateStaff(int ID, UpdateStaffReqDTO dtoObj)
        {
            var result = new DataTransferObject<StaffResDTO>();

            var foundStaff = await _repository.Find(v => v.ID == ID && v.isActive);

            if (foundStaff == null)
            {
                result = new DataTransferObject<StaffResDTO>(true, 0, Constant.ResponseMessage.STAFF_NOT_FOUND);
                return result;
            }

            foundStaff.Name = dtoObj.Name;
            foundStaff.Address = dtoObj.Address;
            foundStaff.Email = dtoObj.Email;
            foundStaff.Phone2 = dtoObj.Phone2;

            await this.UpdateEntityAsync(foundStaff);


            var staffObj = _mapper.Map<StaffResDTO>(foundStaff);
            result = new DataTransferObject<StaffResDTO>(staffObj, 1, Constant.ResponseMessage.UPDATE_STAFF);
            return result;



        }

        public async Task<DataTransferObject<StaffResDTO>> DeleteStaff(int staffID)
        {
            var result = new DataTransferObject<StaffResDTO>();

            var foundStaff = await _repository.Find(v => v.ID == staffID && v.isActive);

            if (foundStaff == null)
            {
                result = new DataTransferObject<StaffResDTO>(true, 0, Constant.ResponseMessage.STAFF_NOT_FOUND);
                return result;
            }

            foundStaff.isActive = false;
            await this.UpdateEntityAsync(foundStaff);

            result = new DataTransferObject<StaffResDTO>(1, Constant.ResponseMessage.DELETE_STAFF);
            return result;



        }
    }
}
