﻿using AutoMapper;
using InventoryManagement.Common;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Generic;
using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Services
{
    public class ProductService : Service<IProductRepository, Product, int>, IProductService
    {
        private readonly IProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IUserService _userService;


        public ProductService(IUnitOfWork unitOfWork
            , IProductRepository repository
            , IMapper mapper
            , IRefreshTokenRepository refreshTokenRepository
            , IUserService userService

            ) : base(unitOfWork, repository, mapper)
        {
            _mapper = mapper;

            _repository = repository;
            _refreshTokenRepository = refreshTokenRepository;
            _userService = userService;

        }

        public async Task<DataTransferObject<ProductResDTO>> AddProduct(AddProductReqDTO dtoObj)
        {
            var result = new DataTransferObject<ProductResDTO>();

            var foundProduct = await _repository.Find(v => v.Code == dtoObj.Code);
            if (foundProduct != null)
            {
                result = new DataTransferObject<ProductResDTO>(true, 0, Constant.ResponseMessage.PRODUCT_EXIST);
                return result;
            }

            var productObj = _mapper.Map<Product>(dtoObj);
            productObj.isActive = true;
            await _repository.Create(productObj);


            var resObj = _mapper.Map<ProductResDTO>(productObj);

            result = new DataTransferObject<ProductResDTO>(resObj, 1, Constant.ResponseMessage.PRODUCT_ADDED);
            return result;

        }

        public async Task<DataTransferObject<ProductResDTO>> GetProduct(int productID)
        {

            var result = new DataTransferObject<ProductResDTO>();

            var foundProduct = await _repository.Find(v => v.ID == productID && v.isActive);

            if (foundProduct == null)
            {
                result = new DataTransferObject<ProductResDTO>(true, 0, Constant.ResponseMessage.PRODUCT_NOT_FOUND);
                return result;
            }

            var resObj = _mapper.Map<ProductResDTO>(foundProduct);
            result = new DataTransferObject<ProductResDTO>(resObj);
            return result;
        }

        public async Task<DataTransferObject<List<ProductResDTO>>> GetAllProduct()
        {

            var result = new DataTransferObject<List<ProductResDTO>>();

            var foundProduct = await _repository.FindAll(v => v.isActive);

            if (foundProduct == null)
            {
                result = new DataTransferObject<List<ProductResDTO>>(true, 0, Constant.ResponseMessage.PRODUCT_NOT_FOUND);
                return result;
            }

            List<ProductResDTO> productList = new List<ProductResDTO>();
            foreach (var product in foundProduct)
            {
                productList.Add(_mapper.Map<ProductResDTO>(product));
            }


            result = new DataTransferObject<List<ProductResDTO>>(productList);
            return result;
        }

        //public async Task<DataTransferObject<ProductResDTO>> UpdateProduct(int ID, UpdateCustomerReqDTO dtoObj)
        //{
        //    var result = new DataTransferObject<ProductResDTO>();

        //    var foundProduct = await _repository.Find(v => v.ID == ID && v.isActive);

        //    if (foundProduct == null)
        //    {
        //        result = new DataTransferObject<ProductResDTO>(true, 0, Constant.ResponseMessage.PRODUCT_NOT_FOUND);
        //        return result;
        //    }

        //    foundProduct.Name = dtoObj.Name;
        //    foundProduct.Address = dtoObj.Address;
        //    foundProduct.Email = dtoObj.Email;
        //    foundProduct.Phone2 = dtoObj.Phone2;

        //    await this.UpdateEntityAsync(foundProduct);


        //    var customerObj = _mapper.Map<ProductResDTO>(foundProduct);
        //    result = new DataTransferObject<ProductResDTO>(customerObj, 1, Constant.ResponseMessage.UPDATE_CUSTOMER);
        //    return result;



        //}

        public async Task<DataTransferObject<ProductResDTO>> DeleteProduct(int productID)
        {
            var result = new DataTransferObject<ProductResDTO>();

            var foundProduct = await _repository.Find(v => v.ID == productID && v.isActive);

            if (foundProduct == null)
            {
                result = new DataTransferObject<ProductResDTO>(true, 0, Constant.ResponseMessage.PRODUCT_NOT_FOUND);
                return result;
            }

            foundProduct.isActive = false;
            await this.UpdateEntityAsync(foundProduct);

            result = new DataTransferObject<ProductResDTO>(1, Constant.ResponseMessage.DELETE_PRODUCT);
            return result;



        }
    }
}
