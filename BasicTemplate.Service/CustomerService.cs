﻿using AutoMapper;
using InventoryManagement.Common;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Generic;
using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Services
{
    public class CustomerService : Service<ICustomerRepository, Customer, int>, ICustomerService
    {
        private readonly ICustomerRepository _repository;
        private readonly IMapper _mapper;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IUserService _userService;


        public CustomerService(IUnitOfWork unitOfWork
            , ICustomerRepository repository
            , IMapper mapper
            , IRefreshTokenRepository refreshTokenRepository
            , IUserService userService

            ) : base(unitOfWork, repository, mapper)
        {
            _mapper = mapper;

            _repository = repository;
            _refreshTokenRepository = refreshTokenRepository;
            _userService = userService;

        }

        public async Task<DataTransferObject<CustomerResDTO>> AddCustomer(AddCustomerReqDTO dtoObj)
        {
            var result = new DataTransferObject<CustomerResDTO>();

            var foundCustomer = await _repository.Find(v => v.Phone == dtoObj.Phone);
            if (foundCustomer != null)
            {
                result = new DataTransferObject<CustomerResDTO>(true, 0, Constant.ResponseMessage.CUSTOMER_EXIST);
                return result;
            }

            var customerObj = _mapper.Map<Customer>(dtoObj);
            customerObj.isActive = true;
            await _repository.Create(customerObj);


            var resObj = _mapper.Map<CustomerResDTO>(customerObj);

            result = new DataTransferObject<CustomerResDTO>(resObj, 1, Constant.ResponseMessage.CUSTOMER_ADDED);
            return result;

        }

        public async Task<DataTransferObject<CustomerResDTO>> GetCustomer(int customerID)
        {

            var result = new DataTransferObject<CustomerResDTO>();

            var foundCustomer = await _repository.Find(v => v.ID == customerID && v.isActive);

            if (foundCustomer == null)
            {
                result = new DataTransferObject<CustomerResDTO>(true, 0, Constant.ResponseMessage.CUSTOMER_NOT_FOUND);
                return result;
            }

            var resObj = _mapper.Map<CustomerResDTO>(foundCustomer);
            result = new DataTransferObject<CustomerResDTO>(resObj);
            return result;
        }

        public async Task<DataTransferObject<List<CustomerResDTO>>> GetAllCustomer()
        {

            var result = new DataTransferObject<List<CustomerResDTO>>();

            var foundCustomers = await _repository.FindAll(v => v.isActive);

            if (foundCustomers == null)
            {
                result = new DataTransferObject<List<CustomerResDTO>>(true, 0, Constant.ResponseMessage.CUSTOMER_NOT_FOUND);
                return result;
            }

            List<CustomerResDTO> customerList = new List<CustomerResDTO>();
            foreach (var customer in foundCustomers)
            {
                customerList.Add(_mapper.Map<CustomerResDTO>(customer));
            }


            result = new DataTransferObject<List<CustomerResDTO>>(customerList);
            return result;
        }

        public async Task<DataTransferObject<CustomerResDTO>> UpdateCustomer(int ID, UpdateCustomerReqDTO dtoObj)
        {
            var result = new DataTransferObject<CustomerResDTO>();

            var foundCustomer = await _repository.Find(v => v.ID == ID && v.isActive);

            if (foundCustomer == null)
            {
                result = new DataTransferObject<CustomerResDTO>(true, 0, Constant.ResponseMessage.CUSTOMER_NOT_FOUND);
                return result;
            }

            foundCustomer.Name = dtoObj.Name;
            foundCustomer.Address = dtoObj.Address;
            foundCustomer.Email = dtoObj.Email;
            foundCustomer.Phone2 = dtoObj.Phone2;

            await this.UpdateEntityAsync(foundCustomer);


            var customerObj = _mapper.Map<CustomerResDTO>(foundCustomer);
            result = new DataTransferObject<CustomerResDTO>(customerObj, 1, Constant.ResponseMessage.UPDATE_CUSTOMER);
            return result;



        }

        public async Task<DataTransferObject<CustomerResDTO>> DeleteCustomer(int customerID)
        {
            var result = new DataTransferObject<CustomerResDTO>();

            var foundCustomer = await _repository.Find(v => v.ID == customerID && v.isActive);

            if (foundCustomer == null)
            {
                result = new DataTransferObject<CustomerResDTO>(true, 0, Constant.ResponseMessage.CUSTOMER_NOT_FOUND);
                return result;
            }

            foundCustomer.isActive = false;
            await this.UpdateEntityAsync(foundCustomer);

            result = new DataTransferObject<CustomerResDTO>(1, Constant.ResponseMessage.DELETE_CUSTOMER);
            return result;



        }
    }
}
