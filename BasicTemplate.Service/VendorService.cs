﻿using AutoMapper;
using InventoryManagement.Common;
using InventoryManagement.Core.DTO.Request;
using InventoryManagement.Core.DTO.Response;
using InventoryManagement.Core.Entity;
using InventoryManagement.Core.IRepository;
using InventoryManagement.Core.IService;
using InventoryManagement.NetCore.Base.Abstract;
using InventoryManagement.NetCore.Base.Generic;
using InventoryManagement.NetCore.Base.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManagement.Services
{
    public class VendorService : Service<IVendorRepository, Vendor, int>, IVendorService
    {
        private readonly IVendorRepository _repository;
        private readonly IMapper _mapper;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IUserService _userService;


        public VendorService(IUnitOfWork unitOfWork
            , IVendorRepository repository
            , IMapper mapper
            , IRefreshTokenRepository refreshTokenRepository
            , IUserService userService

            ) : base(unitOfWork, repository, mapper)
        {
            _mapper = mapper;

            _repository = repository;
            _refreshTokenRepository = refreshTokenRepository;
            _userService = userService;

        }

        public async Task<DataTransferObject<VendorResDTO>> AddVendor(AddVendorReqDTO dtoObj)
        {
            var result = new DataTransferObject<VendorResDTO>();

            var foundVendor = await _repository.Find(v => v.Phone == dtoObj.Phone);
            if (foundVendor != null)
            {
                result = new DataTransferObject<VendorResDTO>(true, 0, Constant.ResponseMessage.VENDOR_EXIST);
                return result;
            }

            var vendorObj = _mapper.Map<Vendor>(dtoObj);
            vendorObj.isActive = true;
            await _repository.Create(vendorObj);


            var resObj = _mapper.Map<VendorResDTO>(vendorObj);

            result = new DataTransferObject<VendorResDTO>(resObj, 1, Constant.ResponseMessage.VENDOR_ADDED);
            return result;

        }

        public async Task<DataTransferObject<VendorResDTO>> GetVendor(int vendorID)
        {

            var result = new DataTransferObject<VendorResDTO>();

            var foundVendor = await _repository.Find(v => v.ID == vendorID && v.isActive);

            if (foundVendor == null)
            {
                result = new DataTransferObject<VendorResDTO>(true, 0, Constant.ResponseMessage.VENDOR_NOT_FOUND);
                return result;
            }

            var resObj = _mapper.Map<VendorResDTO>(foundVendor);
            result = new DataTransferObject<VendorResDTO>(resObj);
            return result;
        }

        public async Task<DataTransferObject<List<VendorResDTO>>> GetAllVendor()
        {

            var result = new DataTransferObject<List<VendorResDTO>>();

            var foundVendors = await _repository.FindAll(v => v.isActive);

            if (foundVendors == null)
            {
                result = new DataTransferObject<List<VendorResDTO>>(true, 0, Constant.ResponseMessage.VENDOR_NOT_FOUND);
                return result;
            }

            List<VendorResDTO> vendorList = new List<VendorResDTO>();
            foreach (var vendor in foundVendors)
            {
                vendorList.Add(_mapper.Map<VendorResDTO>(vendor));
            }


            result = new DataTransferObject<List<VendorResDTO>>(vendorList);
            return result;
        }

        public async Task<DataTransferObject<VendorResDTO>> UpdateVendor(int ID, UpdateVendorReqDTO dtoObj)
        {
            var result = new DataTransferObject<VendorResDTO>();

            var foundVendor = await _repository.Find(v => v.ID == ID && v.isActive);

            if (foundVendor == null)
            {
                result = new DataTransferObject<VendorResDTO>(true, 0, Constant.ResponseMessage.VENDOR_NOT_FOUND);
                return result;
            }

            foundVendor.Name = dtoObj.Name;
            foundVendor.Address = dtoObj.Address;
            foundVendor.Company = dtoObj.Company;
            foundVendor.Email = dtoObj.Email;
            foundVendor.Phone2 = dtoObj.Phone2;
            foundVendor.Office = dtoObj.Office;

            await this.UpdateEntityAsync(foundVendor);


            var vendorObj = _mapper.Map<VendorResDTO>(foundVendor);
            result = new DataTransferObject<VendorResDTO>(vendorObj, 1, Constant.ResponseMessage.UPDATE_VENDOR);
            return result;



        }

        public async Task<DataTransferObject<VendorResDTO>> DeleteVendor(int vendorID)
        {
            var result = new DataTransferObject<VendorResDTO>();

            var foundVendor = await _repository.Find(v => v.ID == vendorID && v.isActive);

            if (foundVendor == null)
            {
                result = new DataTransferObject<VendorResDTO>(true, 0, Constant.ResponseMessage.VENDOR_NOT_FOUND);
                return result;
            }

            foundVendor.isActive = false;
            await this.UpdateEntityAsync(foundVendor);

            result = new DataTransferObject<VendorResDTO>(1, Constant.ResponseMessage.DELETE_VENDOR);
            return result;



        }
    }
}
